import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Pagina2RoutingModule } from './pagina2-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Pagina2RoutingModule
  ]
})
export class Pagina2Module { }
