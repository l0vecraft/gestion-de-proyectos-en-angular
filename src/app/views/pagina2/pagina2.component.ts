import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ChildActivationStart } from '@angular/router';
@Component({
  selector: 'Pagina2',
  templateUrl: './pagina2.component.html',
  styleUrls: ['./pagina2.component.scss']
})
export class Pagina2Component implements OnInit {
  public nombre:String;
  public apellido:String;
  constructor(
    private _route: ActivatedRoute,// con este podemos sacar parametros
    private _router: Router// y este para realizar redirecciones
  ) {

   }

  ngOnInit(): void {
    this._route.params.subscribe((params:Params)=>{
      this.nombre = params.nombre
      this.apellido = params.apellido
    })
  }
  
  redirect():void{
    // haciendo redirecciones con parametros
    // this._router.navigate(['/formulario']) normal
    this._router.navigate(['/formulario', 'erick','Pico'])
  }
  addName():void {
    this.nombre = 'erick';
    this.apellido = 'Pico';
  }

}
