import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'Formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
  public user:any;
  public biografia:string;
  constructor() { 
    this.user = {
      nombre: '',
      apellidos: '',
      bio: '',
      genero: ''
    }
    this.biografia = '';
  }

  ngOnInit(): void {
  }
  onSubmit(): void {
    console.log(this.user)
  }

}
