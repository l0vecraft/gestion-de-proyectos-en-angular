import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Pagina1RoutingModule } from './pagina1-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Pagina1RoutingModule
  ]
})
export class Pagina1Module { }
