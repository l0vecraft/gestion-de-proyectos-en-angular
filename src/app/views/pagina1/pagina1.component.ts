import { Component, OnInit } from '@angular/core';
import {Pelicula} from '../../core/models/Pelicula';
@Component({
  selector: 'Pagina1',
  templateUrl: './pagina1.component.html',
  styleUrls: ['./pagina1.component.scss']
})
export class Pagina1Component implements OnInit {
  public listaPeliculas:Array<Pelicula>;
  public peliculaFav: Pelicula;
  constructor() { 
    this.listaPeliculas = [
      new Pelicula('spiderman Homecoming',2015,'https://i.pinimg.com/originals/c8/66/93/c86693302b6981209afe2def102d0668.jpg'),
      new Pelicula('los vengadores End game',2019,'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/poster-vengadores-endgame-1552567490.jpg?crop=1xw:1xh;center,top&resize=480:*'),
      new Pelicula('animales fantasticos y donde encontrarlos',2016,'https://www.elperiodic.com/archivos/imagenes/noticias/2019/04/26/cartel.jpg'),
    ]
  }

  ngOnInit(): void {
  }

  fav(evento){
    this.peliculaFav = evento;
    console.log(evento)
  }

}
