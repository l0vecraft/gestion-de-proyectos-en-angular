import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MiComponente } from './components/mi-componente/mi-componente.component';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { NavBarComponent } from './components/navBar/navbar.component';
import { FootComponent } from './components/foot/foot.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { SliderComponent } from './components/slider/slider.component';
import { HomeComponent } from './views/home/home.component';
import { BlogComponent } from './views/blog/blog.component';
import { FormularioComponent } from './views/formulario/formulario.component';
import { Pagina1Component } from './views/pagina1/pagina1.component';
import { Pagina2Component } from './views/pagina2/pagina2.component';
import { ErrorComponent } from './views/error/error.component';
import { PeliculaComponent } from './components/pelicula/pelicula.component';

import {Capitalize} from './core/pipes/capitalize.pipe';
import { HomeModule } from './views/home/home.module'

@NgModule({
  declarations: [
    AppComponent,
    MiComponente,
    PeliculasComponent,
    NavBarComponent,
    FootComponent,
    SideBarComponent,
    SliderComponent,
    HomeComponent,
    BlogComponent,
    FormularioComponent,
    Pagina1Component,
    Pagina2Component,
    ErrorComponent,
    PeliculaComponent,
    Capitalize
  ],
  imports: [
    // Modulos de angular o de terceros
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
