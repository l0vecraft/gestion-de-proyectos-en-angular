import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { BlogComponent } from './views/blog/blog.component';
import { FormularioComponent } from './views/formulario/formulario.component';
import { Pagina1Component } from './views/pagina1/pagina1.component';
import { Pagina2Component } from './views/pagina2/pagina2.component';
import { ErrorComponent } from './views/error/error.component';

const routes: Routes = [
  {path:'home',loadChildren:()=>import('./views/home/home.module').then(m=>m.HomeModule)},
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'blog',loadChildren:()=>import('./views/blog/blog.module').then(m=>m.BlogModule)},
  {path:'formulario',loadChildren:()=>import('./views/formulario/formulario.module').then(m=>m.FormularioModule)},
  {path:'pagina1',loadChildren:()=>import('./views/pagina1/pagina1.module').then(m=>m.Pagina1Module)},
  {path:'pagina2',loadChildren:()=>import('./views/pagina2/pagina2.module').then(m=>m.Pagina2Module)},
  {path:'**',loadChildren:()=>import('./views/error/error.module').then(m=>m.ErrorModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
