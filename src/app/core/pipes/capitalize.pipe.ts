import {Pipe, PipeTransform} from '@angular/core'

@Pipe({
    name: 'capitalize'
})
export class Capitalize implements PipeTransform{
    //debe de llevar el metodo transform o DARA ERROR

    transform(value: string): string{
        return value[0].toUpperCase() + value.slice(1);
    }
    //una vez hecha se debe importar en AppModule
}