import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'Slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  public rutaActual:string
  //esta sera la propiedad que estoy pasando
  @Input() public nombre:string;
  @Input() public size:string;

  constructor(private _router: Router) {
    // this.title = "Bienvenido al curso de Angular"
    this.rutaActual = this._router.url.split('/')[1];
    if(this.nombre === "" || this.nombre === undefined){
      this.nombre = "Bienvenido al curso de Angular"
    }
   }

  ngOnInit(): void {
  }

}
