import { Component, OnInit, DoCheck, OnDestroy, Input } from '@angular/core';
import { Pelicula } from 'src/app/core/models/Pelicula';

@Component({
  selector: 'Peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.scss']
})
export class PeliculasComponent implements OnInit, DoCheck, OnDestroy {
  public titulo:string;
  public peliculas: Array<any>;
  @Input() favorita: Pelicula;
  constructor() {
    this.titulo = 'Componente peliculas';
   }

  ngOnInit(): void {
  }

  ngDoCheck(): void{
    //verifica los cambios que existan en el componente
    console.log('Lanzado el docheck')
  }

  ngOnDestroy():void {
    console.log('El componente se destruira')
  }

  cambiarTitulo():void{
    this.titulo = 'El titulo ha sido cambiado'
  }

  //Mi pipe
  // capitalize(value:string):string{
  //   return value[0].toUpperCase() + value.slice(1);
  // }

}
