import { NgClass } from "@angular/common";

import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'NavBar',
    templateUrl: 'navbar.component.html'
})

export class NavBarComponent implements OnInit {
    public links: Array<any>;
    constructor() { 
        this.links = [
            {title:'Inicio', path:'home'},
            {title:'Blog', path:'blog'},
            {title:'Formulario', path:'formulario'},
            {title:'Pagina 1', path:'pagina1'},
            {title:'Pagina 2', path:'pagina2'},
        ]
    }

    ngOnInit() { }
}