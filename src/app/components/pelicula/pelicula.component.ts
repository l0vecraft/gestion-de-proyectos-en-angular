import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pelicula } from 'src/app/core/models/Pelicula';

@Component({
  selector: 'Pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.scss']
})
export class PeliculaComponent implements OnInit {
  @Input() public peliculas: Array<Pelicula>;
  @Output() MarcarFavorita= new EventEmitter();//objeto evento para comunicarlo con el padre
  constructor() { }

  ngOnInit(): void {
    console.log(this.peliculas)
  }

  favorita(evento, pelicula){
    this.MarcarFavorita.emit(pelicula) //al estilo vue se emite el evento con los datos
  }

}
