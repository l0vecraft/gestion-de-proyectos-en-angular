import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'MiComponente',
  templateUrl: './mi-componente.component.html',
  styleUrls: ['./mi-componente.component.scss']
})
export class MiComponente implements OnInit {
  //declarando variables o propiedades
  public titulo: string;
  public comentario: string;
  public year: number;
  public mostrarPeliculas: boolean;

  constructor() { 
    //aca se definen los valores de las propiedades
    this.titulo = 'hola mundo, este es el componente "Mi Componente"';
    this.comentario = 'este es el comentario'
    this.year = 2020;
    this.mostrarPeliculas = true;

    console.log('hola constructor')
    console.log(this.titulo)
  }

  ngOnInit(): void {
    console.log('hola onInit')
  }
  ocultarPeliculas():void{
    this.mostrarPeliculas = !this.mostrarPeliculas
  }
}
