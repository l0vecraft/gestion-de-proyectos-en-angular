const plugin = require('tailwindcss/plugin')
module.exports = {
  purge: [],
  theme: {
    extend: {
      colors:{
        angularRed: '#fc0313',
        customGreen: '#1ce60e'
      },
      height:{
        '72': '18rem',
        '80': '20rem',
        '96': '24rem',
        '122': '28rem'
      }
    },
  },
  variants: {},
  plugins: [
    plugin(({addUtilities})=>{
      const newUtilities={
        '.red-gradient':{
          background:'rgb(255,0,0)',
          background: 'radial-gradient(circle, rgba(255,0,0,0.8940710073091737) 0%, rgba(187,0,0,1) 100%)' 
        }
      }
      addUtilities(newUtilities)
    })
  ],
}
